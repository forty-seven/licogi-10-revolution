﻿using System.Globalization;
using System.Threading;
using System.Web.SessionState;

namespace LICOGI_10_REVOLUTION.Controllers
{
    public class CultureHelper
    {
        protected HttpSessionState session;

        //constructor
        public CultureHelper(HttpSessionState httpSessionState)
        {
            session = httpSessionState;
        }

        // Properties
        public static int CurrentCulture
        {
            get
            {
                if (Thread.CurrentThread.CurrentUICulture.Name == "vi-VN")
                {
                    return 0;
                }
                else if (Thread.CurrentThread.CurrentUICulture.Name == "en-US")
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (value == 0)
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("vi-VN");
                }
                else if (value == 1)
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                }

                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            }
        }
    }
}