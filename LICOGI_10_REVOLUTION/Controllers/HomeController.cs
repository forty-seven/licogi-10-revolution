﻿using LICOGI_10_REVOLUTION.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ICollection<TB_ARTICAL> listNews;
            ICollection<TB_ARTICAL> listNewsWorld = new List<TB_ARTICAL>();

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listNews = context.TB_ARTICAL.Where(x => x.ArticalType == Constant.ARTICAL_NEWS)
                    .OrderBy(x => x.DisplayOrder)
                    .ThenByDescending(x => x.CreateDate)
                    .Take(5)
                    .ToList();
            }

            listNewsWorld.Add(new TB_ARTICAL
            {
                AvataUrl = @"http://img.f28.kinhdoanh.vnecdn.net/2017/04/19/HNHP-1492616157_490x294.jpg",
                Title = "Chủ đầu tư cao tốc Hà Nội - Hải Phòng lỗ hơn 1.756 tỷ đồng",
                Description = "Sau hơn một năm vận hành, tuyến cao tốc hiện đại nhất Việt Nam đã mang về cho chủ đầu tư 1.430 tỷ đồng doanh thu, song khấu hao lớn ",
                CreateDate = DateTime.Now,
                FriendlyUrl = @"http://kinhdoanh.vnexpress.net/tin-tuc/doanh-nghiep/chu-dau-tu-cao-toc-ha-noi-hai-phong-lo-hon-1-756-ty-dong-3572933.html"
            });

            listNewsWorld.Add(new TB_ARTICAL
            {
                AvataUrl = @"http://img.f25.kinhdoanh.vnecdn.net/2017/04/19/nhamay-TTCS-7842-1492588533.jpg",
                Title = "Hai doanh nghiệp mía đường lớn nhất nước tính chuyện sáp nhập",
                Description = "Kế hoạch hợp nhất Mía đường Thành Thành Công Tây Ninh và Đường Biên Hoà sẽ tạo ra doanh nghiệp có vốn hoá thị trường gần 10.000 tỷ đồng",
                CreateDate = DateTime.Now.AddDays(-1),
                FriendlyUrl = @"http://kinhdoanh.vnexpress.net/tin-tuc/doanh-nghiep/hai-doanh-nghiep-mia-duong-lon-nhat-nuoc-tinh-chuyen-sap-nhap-3572707.html"
            });

            ViewBag.listNews = listNews;
            ViewBag.listNewsWorld = listNewsWorld;

            return View();
        }

        public ActionResult ThiCongCoGioi()
        {
            ViewBag.content = GetFixContent(Constant.THI_CONG_CO_GIOI);
            return View();
        }

        public ActionResult ThiCongXayLap()
        {
            ViewBag.content = GetFixContent(Constant.THI_CONG_XAY_LAP);
            return View();
        }

        public ActionResult ThietBiThiCong()
        {
            string localeName = Thread.CurrentThread.CurrentUICulture.Name;
            ICollection<TB_MACHINERIES> list;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                list = context.TB_MACHINERIES
                    .OrderBy(x => x.CreateDate)
                    .ToList();
            }
            if (localeName.Equals("en-US"))
            {
                foreach (TB_MACHINERIES item in list)
                {
                    item.Name = !String.IsNullOrEmpty(item.NameEn) ? item.NameEn : item.Name;
                    item.MadeIn = !String.IsNullOrEmpty(item.MadeInEn) ? item.MadeInEn : item.MadeIn;
                    item.Power = !String.IsNullOrEmpty(item.PowerEn) ? item.PowerEn : item.Power;
                    item.DetailInfo = !String.IsNullOrEmpty(item.DetailInfo) ? WebUtility.HtmlDecode(item.DetailInfoEn) : WebUtility.HtmlDecode(item.DetailInfo);
                }
            }

            ViewBag.listMachine = list;
            return View();
        }

        public ActionResult DuAn()
        {
            string localeName = Thread.CurrentThread.CurrentUICulture.Name;
            ICollection<TB_ARTICAL> listProject;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listProject = context.TB_ARTICAL
                    .Where(x => x.ArticalType == Constant.ARTICAL_PROJECT)
                    .OrderBy(x => x.DisplayOrder)
                    .Take(20)
                    .ToList();
            }

            if (localeName.Equals("en-US"))
            {
                foreach (TB_ARTICAL obj in listProject)
                {
                    obj.Title = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                    obj.Description = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                    obj.FriendlyUrl = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                }
            }

            ViewBag.listProject = listProject;
            return View();
        }

        public ActionResult VatLieuXayDung()
        {
            ViewBag.content = GetFixContent(Constant.VAT_LIEU_XAY_DUNG);
            return View();
        }

        public ActionResult LapDatNhomKinh()
        {
            ViewBag.content = GetFixContent(Constant.CUA_KINH_SON_TINH_DIEN);
            return View();
        }

        public ActionResult DoanTheXaHoi()
        {
            ICollection<TB_ARTICAL> listNews;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listNews = context.TB_ARTICAL.Where(x => x.ArticalType == Constant.ARTICAL_YOUTH).OrderBy(x => x.DisplayOrder).ThenByDescending(x => x.CreateDate).ToList();
            }

            string localeName = Thread.CurrentThread.CurrentUICulture.Name;

            if (localeName.Equals("en-US"))
            {
                foreach (TB_ARTICAL obj in listNews)
                {
                    obj.Title = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                    obj.Description = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                    obj.FriendlyUrl = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                }
            }

            ViewBag.listNews = listNews;

            return View();
        }

        public ActionResult VanBanPhapQuy()
        {
            ViewBag.content = GetFixContent(Constant.VAN_BAN_PHAP_QUY);
            return View();
        }

        public ActionResult DownloadFile(int id)
        {
            byte[] fileBytes = null;
            var cd = new System.Net.Mime.ContentDisposition();
            if (id == 1)
            {
                var path = Server.MapPath(@"~\Uploads\Files\NHOM_KINH.pdf");
                cd = new System.Net.Mime.ContentDisposition
                {
                    // for example foo.bak
                    FileName = path.Substring(path.LastIndexOf("\\", StringComparison.Ordinal)),

                    // always prompt the user for downloading, set to true if you want
                    // the browser to try to show the file inline
                    Inline = false,
                };
                fileBytes = System.IO.File.ReadAllBytes(path);
            }

            return File(fileBytes, cd.ToString(), "NHOM_KINH.pdf");
        }

        public ActionResult Aside(bool displayNews, bool displayProject)
        {
            ICollection<TB_ARTICAL> listNews = new List<TB_ARTICAL>();
            ICollection<TB_ARTICAL> listProject = new List<TB_ARTICAL>();

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                if (displayNews)
                {
                    listNews = context.TB_ARTICAL
                   .Where(x => x.ArticalType == Constant.ARTICAL_NEWS)
                   .OrderBy(x => x.DisplayOrder)
                   .ThenByDescending(x => x.CreateDate)
                   .Take(5)
                   .ToList();
                }

                if (displayProject)
                {
                    listProject = context.TB_ARTICAL
                    .Where(x => x.ArticalType == Constant.ARTICAL_PROJECT)
                    .OrderBy(x => x.DisplayOrder)
                    .ThenByDescending(x => x.CreateDate)
                    .Take(5)
                    .ToList();
                }
            }

            ViewBag.listNews = listNews;
            ViewBag.listProject = listProject;
            return View();
        }

        public ActionResult Aside2()
        {
            ICollection<TB_ARTICAL> listNews;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listNews = context.TB_ARTICAL
                    .Where(x => x.ArticalType == Constant.ARTICAL_NEWS)
                    .OrderBy(x => x.DisplayOrder)
                    .ThenByDescending(x => x.CreateDate)
                    .Take(5)
                    .ToList();
            }

            ViewBag.listNews = listNews;
            return View();
        }

        public ActionResult ChiTietBaiViet(int id)
        {
            TB_ARTICAL model;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                model = context.TB_ARTICAL.FirstOrDefault(x => x.Id == id);
            }

            if (model == null)
            {
                return RedirectToAction("ErrorInfo", "Error", new { message = "Bài viết không tồn tại" });
            }

            string localeName = Thread.CurrentThread.CurrentUICulture.Name;

            if (localeName.Equals("en-US"))
            {
                model.Title = !String.IsNullOrEmpty(model.TitleEn) ? model.TitleEn : model.Title;
                model.ContentText = !String.IsNullOrEmpty(model.ContentTextEn) ? WebUtility.HtmlDecode(model.ContentTextEn) : WebUtility.HtmlDecode(model.ContentText);
            }
            else
            {
                model.Title = model.Title;
                model.ContentText = WebUtility.HtmlDecode(model.ContentText);
            }

            return View(model);
        }

        public ActionResult TinTuc()
        {
            ICollection<TB_ARTICAL> listNews;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listNews = context.TB_ARTICAL.Where(x => x.ArticalType == Constant.ARTICAL_NEWS).OrderBy(x => x.DisplayOrder).ThenByDescending(x => x.CreateDate).ToList();
            }

            string localeName = Thread.CurrentThread.CurrentUICulture.Name;

            if (localeName.Equals("en-US"))
            {
                foreach (TB_ARTICAL obj in listNews)
                {
                    obj.Title = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                    obj.Description = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                    obj.FriendlyUrl = !String.IsNullOrEmpty(obj.TitleEn) ? obj.TitleEn : obj.Title;
                }
            }

            ViewBag.listNews = listNews;

            return View();
        }

        public ActionResult GioiThieu()
        {
            ViewBag.content1 = GetIntroduce(1);
            ViewBag.content2 = GetIntroduce(2);
            ViewBag.content3 = GetIntroduce(3);
            ViewBag.content4 = GetIntroduce(4);
            ViewBag.content5 = GetIntroduce(5);
            return View();
        }

        public ActionResult ChangeCurrentCulture(int id)
        {
            //
            // Change the current culture for this user.
            //
            CultureHelper.CurrentCulture = id;
            //
            // Cache the new current culture into the user HTTP session.
            //
            Session["CurrentCulture"] = id;
            //
            // Redirect to the same page from where the request was made!
            //
            return Redirect(Request.UrlReferrer.ToString());
        }

        private string GetFixContent(int pageId)
        {
            string content = null;
            string localeName = Thread.CurrentThread.CurrentUICulture.Name;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                TB_FIX_PAGE_CONTENT contentObject = context.TB_FIX_PAGE_CONTENT.FirstOrDefault(x => x.Id == pageId);
                if (localeName.Equals("en-US"))
                {
                    if (contentObject != null)
                        content = !String.IsNullOrEmpty(contentObject.ContentTextEn)
                            ? contentObject.ContentTextEn
                            : contentObject.ContentText;
                }
                else
                {
                    if (contentObject != null) content = contentObject.ContentText;
                }
            }

            return WebUtility.HtmlDecode(content ?? throw new InvalidOperationException());
        }

        private string GetIntroduce(int pageId)
        {
            string content = null;
            string localeName = Thread.CurrentThread.CurrentUICulture.Name;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                TB_INTRODUCE contentObject = context.TB_INTRODUCE.FirstOrDefault(x => x.Id == pageId);
                if (localeName.Equals("en-US"))
                {
                    if (contentObject != null)
                        content = !String.IsNullOrEmpty(contentObject.ContentTextEn)
                            ? contentObject.ContentTextEn
                            : contentObject.ContentText;
                }
                else
                {
                    if (contentObject != null) content = contentObject.ContentText;
                }
            }

            return WebUtility.HtmlDecode(content ?? throw new InvalidOperationException());
        }
    }
}