﻿using LICOGI_10_REVOLUTION.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Controllers
{
    public class AdminController : Controller
    {
        [LoginFilter]
        public ActionResult Solution(int? id)
        {
            // From id = 7, is child menu of Solution id = 6
            // 6,7,8,9,10 -> introduce page

            SolutionModel model = new SolutionModel {ListPageModel = new List<PageModel>()};
            if (id == null)
            {
                id = 6;
            }
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                if (id != 6)
                {
                    model.ListPageModel.Add(context.TB_FIX_PAGE_CONTENT.Where(x => x.Id == id)
                     .Select(x => new PageModel
                     {
                         Id = x.Id,
                         Name = x.Name,
                         ContentText = x.ContentText,
                         ContentTextEn = x.ContentTextEn
                     }).FirstOrDefault());
                }
                else
                {
                    model.ListPageModel.AddRange(context.TB_FIX_PAGE_CONTENT.Where(x => x.Id == 6)
                                        .Select(x => new PageModel
                                        {
                                            Id = x.Id,
                                            Name = x.Name,
                                            ContentText = x.ContentText,
                                            ContentTextEn = x.ContentTextEn
                                        }).ToList());

                    model.ListPageModel.AddRange(context.TB_INTRODUCE.Select(x => new PageModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        ContentText = x.ContentText,
                        ContentTextEn = x.ContentTextEn
                    }).ToList());
                }

                model.listPage = context.TB_FIX_PAGE_CONTENT.Select(x => new PageModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSolution(PageModel model)
        {
            int result = 0;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                var page = context.TB_FIX_PAGE_CONTENT.FirstOrDefault(x => x.Id == model.Id);

                if (page != null)
                {
                    page.ContentText = model.ContentText;
                    page.ContentTextEn = model.ContentTextEn;

                    context.TB_FIX_PAGE_CONTENT.Attach(page);
                    var entry = context.Entry(page);

                    entry.Property(x => x.ContentText).IsModified = true;
                    entry.Property(x => x.ContentTextEn).IsModified = true;

                    result = context.SaveChanges();
                }
            }

            if (result == 0)
            {
            }

            return RedirectToAction("Solution", new { model.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSolutionIntroduce(List<PageModel> listPageModel)
        {
            int result;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                // remove first null page listPageModel -> 0,1,2,3,4,5
                listPageModel.RemoveAt(0);
                // 0,1,2,34
                var pages = context.TB_INTRODUCE.ToList();

                for (int i = 0; i < 5; i++)
                {
                    pages[i].ContentText = listPageModel[i].ContentText;
                    pages[i].ContentTextEn = listPageModel[i].ContentTextEn;

                    context.TB_INTRODUCE.Attach(pages[i]);
                    var entry = context.Entry(pages[i]);

                    entry.Property(x => x.ContentText).IsModified = true;
                    entry.Property(x => x.ContentTextEn).IsModified = true;

                    context.SaveChanges();
                }

                result = 5;
            }

            if (result == 0)
            {
            }

            return RedirectToAction("Solution", new { Id = 6 });
        }

        [LoginFilter]
        public ActionResult Project()
        {
            ICollection<TB_ARTICAL> listArtical;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listArtical = context.TB_ARTICAL
                    .Where(x => x.ArticalType == Constant.ARTICAL_PROJECT)
                    .OrderByDescending(x => x.CreateDate)
                    .ToList();
            }

            return View(listArtical);
        }

        [LoginFilter]
        public ActionResult News()
        {
            ICollection<TB_ARTICAL> listArtical;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listArtical = context.TB_ARTICAL
                    .Where(x => x.ArticalType == Constant.ARTICAL_NEWS || x.ArticalType == Constant.ARTICAL_YOUTH)
                    .OrderByDescending(x => x.CreateDate)
                    .ToList();
            }

            return View(listArtical);
        }

        [LoginFilter]
        public ActionResult NewArticalView(byte articalType)
        {
            ArticalModel model = new ArticalModel
            {
                ArticalType = articalType,
                ListArticalType = GetArticalTypeDropDown(),
                ListSolutionType = GetSolutionTypeDropDown(),
                IsActive = true
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewArtical(ArticalModel model)
        {
            int result;
            byte articalType = model.ArticalType;

            if (model.ArticalType != 1)
            {
                ModelState.Remove("SolutionType");
            }

            if (!ModelState.IsValid)
            {
                model.ListArticalType = GetArticalTypeDropDown();
                model.ListSolutionType = GetSolutionTypeDropDown();
                return View("NewArticalView", model);
            }

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                var artical = new TB_ARTICAL
                {
                    Title = model.Title,
                    TitleEn = model.TitleEn,
                    ArticalType = model.ArticalType,
                    SolutionType = model.SolutionType,
                    AvataUrl = model.AvataUrl.Replace(@"/Images/", @"/ImageThumbs/"),
                    Description = model.Description,
                    DescriptionEn = model.DescriptionEn,
                    FriendlyUrl = model.FriendlyUrl,
                    FriendlyUrlEn = model.FriendlyUrlEn,
                    DisplayOrder = model.DisplayOrder,
                    IsProjectDone = model.IsProjectDone,
                    IsActive = model.IsActive,
                    ContentText = model.ContentText,
                    ContentTextEn = model.ContentTextEn,
                    CreateDate = DateTime.Now
                };

                context.TB_ARTICAL.Add(artical);
                result = context.SaveChanges();
            }

            if (result == 0)
            {
                model.ListArticalType = GetArticalTypeDropDown();
                model.ListSolutionType = GetSolutionTypeDropDown();
                TempData["MESSAGE"] = "Hệ thống gặp vấn đề, xin thử lại sau";
                return View("NewArticalView", model);
            }

            TempData["MESSAGE"] = "Thêm thành công";
            if (articalType == 1)
            {
                return RedirectToAction("Project");
            }
            else if (articalType == 2)
            {
                return RedirectToAction("News");
            }
            else
            {
                return RedirectToAction("News");
            }
        }

        [LoginFilter]
        public ActionResult EditArticalView(int id)
        {
            ArticalModel model;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                model = context.TB_ARTICAL
                    .Where(x => x.Id == id)
                    .Select(x => new ArticalModel
                    {
                        Id = x.Id,
                        ArticalType = x.ArticalType,
                        SolutionType = x.SolutionType,
                        Title = x.Title,
                        TitleEn = x.TitleEn,
                        AvataUrl = x.AvataUrl,
                        Description = x.Description,
                        DescriptionEn = x.DescriptionEn,
                        ContentText = x.ContentText,
                        ContentTextEn = x.ContentTextEn,
                        FriendlyUrl = x.FriendlyUrl,
                        FriendlyUrlEn = x.FriendlyUrlEn,
                        DisplayOrder = x.DisplayOrder,
                        IsProjectDone = x.IsProjectDone.Value,
                        IsActive = x.IsActive
                    }).FirstOrDefault();
            }
            if (model != null)
            {
                model.ListArticalType = GetArticalTypeDropDown();
                model.ListSolutionType = GetSolutionTypeDropDown();
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditArtical(ArticalModel model)
        {
            int result;
            byte articalType = model.ArticalType;

            if (model.ArticalType != 1)
            {
                ModelState.Remove("SolutionType");
            }

            if (!ModelState.IsValid)
            {
                model.ListArticalType = GetArticalTypeDropDown();
                model.ListSolutionType = GetSolutionTypeDropDown();
                return View("EditArticalView", model);
            }

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                var artical = context.TB_ARTICAL.FirstOrDefault(x => x.Id == model.Id);

                if (artical != null)
                {
                    artical.Title = model.Title;
                    artical.TitleEn = model.TitleEn;
                    artical.ArticalType = model.ArticalType;
                    artical.SolutionType = model.SolutionType;
                    artical.AvataUrl = model.AvataUrl.Replace(@"/Images/", @"/ImageThumbs/");
                    artical.Description = model.Description;
                    artical.DescriptionEn = model.DescriptionEn;
                    artical.FriendlyUrl = model.FriendlyUrl;
                    artical.FriendlyUrlEn = model.FriendlyUrlEn;
                    artical.DisplayOrder = model.DisplayOrder;
                    artical.IsProjectDone = model.IsProjectDone;
                    artical.IsActive = model.IsActive;
                    artical.ContentText = model.ContentText;
                    artical.ContentTextEn = model.ContentTextEn;

                    context.TB_ARTICAL.Attach(artical);
                    var entry = context.Entry(artical);

                    entry.Property(x => x.Title).IsModified = true;
                    entry.Property(x => x.TitleEn).IsModified = true;
                    entry.Property(x => x.ArticalType).IsModified = true;
                    entry.Property(x => x.SolutionType).IsModified = true;
                    entry.Property(x => x.AvataUrl).IsModified = true;
                    entry.Property(x => x.Description).IsModified = true;
                    entry.Property(x => x.DescriptionEn).IsModified = true;
                    entry.Property(x => x.FriendlyUrl).IsModified = true;
                    entry.Property(x => x.FriendlyUrlEn).IsModified = true;
                    entry.Property(x => x.DisplayOrder).IsModified = true;
                    entry.Property(x => x.IsProjectDone).IsModified = true;
                    entry.Property(x => x.IsActive).IsModified = true;
                    entry.Property(x => x.ContentText).IsModified = true;
                    entry.Property(x => x.ContentTextEn).IsModified = true;

                    result = context.SaveChanges();
                }
                else
                {
                    model.ListArticalType = GetArticalTypeDropDown();
                    model.ListSolutionType = GetSolutionTypeDropDown();
                    TempData["MESSAGE"] = "Dự án không tồn tại hoặc đã bị xóa";
                    return View("EditArticalView", model);
                }
            }

            if (result == 0)
            {
                model.ListArticalType = GetArticalTypeDropDown();
                model.ListSolutionType = GetSolutionTypeDropDown();
                TempData["MESSAGE"] = "Hệ thống gặp vấn đề, xin thử lại sau";
                return View("EditArticalView", model);
            }

            TempData["MESSAGE"] = "Cập nhật thành công";
            if (articalType == 1)
            {
                return RedirectToAction("Project");
            }
            else if (articalType == 2)
            {
                return RedirectToAction("News");
            }
            else
            {
                return RedirectToAction("News");
            }
        }

        public ActionResult DeleteArtical(int id)
        {
            byte articalType = 0;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                var artical = context.TB_ARTICAL.FirstOrDefault(x => x.Id == id);
                if (artical != null)
                {
                    articalType = artical.ArticalType;
                    context.TB_ARTICAL.Remove(artical);
                }

                context.SaveChanges();
            }

            TempData["MESSAGE"] = "Xóa thành công";

            if (articalType == 1)
            {
                return RedirectToAction("Project");
            }
            else if (articalType == 2)
            {
                return RedirectToAction("News");
            }
            else
            {
                return RedirectToAction("News");
            }
        }

        public ActionResult HighLight()
        {
            List<HighLightModel> model;
            using (var context = new LOGICO_10_REVOLUTIONEntities())
            {
                model = context.TB_HIGHLIGHT.Select(x => new HighLightModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    ImageUrl = x.ImageUrl,
                    ArticalUrl = x.ArticalUrl,
                    Message = x.Message,
                    MessagePosition = x.MessagePosition.Value,
                    ToTheTop = x.IsActive.Value
                }).ToList();
            }

            return View(model);
        }

        public ActionResult NewHighLight()
        {
            HighLightModel model = new HighLightModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult NewHighLightSubmit(HighLightModel model)
        {
            TB_HIGHLIGHT obj = new TB_HIGHLIGHT
            {
                Name = model.Name,
                ImageUrl = model.ImageUrl,
                ArticalUrl = model.ArticalUrl,
                Message = model.Message,
                MessagePosition = model.MessagePosition,
                IsActive = model.ToTheTop
            };

            using (var context = new LOGICO_10_REVOLUTIONEntities())
            {
                context.TB_HIGHLIGHT.Add(obj);
                context.SaveChanges();
            }
            return RedirectToAction("HighLight");
        }

        public ActionResult Machinery()
        {
            ICollection<TB_MACHINERIES> list;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                list = context.TB_MACHINERIES
                    .OrderByDescending(x => x.CreateDate)
                    .ToList();
            }

            return View(list);
        }

        public ActionResult NewMachineView()
        {
            return View(new MachineModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewMachine(MachineModel model)
        {
            int result;

            if (!ModelState.IsValid)
            {
                return View("NewMachineView", model);
            }

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                var machine = new TB_MACHINERIES
                {
                    Name = model.Name,
                    NameEn = model.NameEn,
                    Count = (short)model.Count,
                    Year = (short)model.Year,

                    MadeIn = model.MadeIn,
                    MadeInEn = model.MadeInEn,
                    Power = model.Power,
                    PowerEn = model.PowerEn,
                    DetailInfo = model.DetailInfo,
                    DetailInfoEn = model.DetailInfoEn,

                    ImageUrl = model.ImageUrl,
                    AvataUrl = model.ImageUrl.Replace(@"/Images/", @"/ImageThumbs/"),
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

                context.TB_MACHINERIES.Add(machine);
                result = context.SaveChanges();
            }

            if (result == 0)
            {
                TempData["MESSAGE"] = "Hệ thống gặp vấn đề, xin thử lại sau";
                return View("NewMachineView", model);
            }

            TempData["MESSAGE"] = "Thêm thành công";
            return RedirectToAction("Machinery");
        }

        public ActionResult EditMachineView(int id)
        {
            MachineModel model;

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                model = context.TB_MACHINERIES
                    .Where(x => x.Id == id)
                    .Select(x => new MachineModel
                    {
                        Id = x.Id,
                        Name = x.Name,
                        NameEn = x.NameEn,
                        Count = (short)x.Count,
                        Year = (short)x.Year,

                        MadeIn = x.MadeIn,
                        MadeInEn = x.MadeInEn,
                        Power = x.Power,
                        PowerEn = x.PowerEn,
                        DetailInfo = x.DetailInfo,
                        DetailInfoEn = x.DetailInfoEn,

                        ImageUrl = x.ImageUrl,
                        AvataUrl = x.AvataUrl,
                        CreateDate = x.CreateDate.Value,
                        UpdateDate = x.UpdateDate.Value
                    }).FirstOrDefault();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMachine(MachineModel model)
        {
            int result;

            if (!ModelState.IsValid)
            {
                return View("EditMachineView", model);
            }

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                var machine = context.TB_MACHINERIES.FirstOrDefault(x => x.Id == model.Id);

                if (machine != null)
                {
                    machine.Name = model.Name;
                    machine.NameEn = model.NameEn;
                    machine.Count = (short)model.Count;
                    machine.Year = (short)model.Year;

                    machine.MadeIn = model.MadeIn;
                    machine.MadeInEn = model.MadeInEn;
                    machine.Power = model.Power;
                    machine.PowerEn = model.PowerEn;
                    machine.DetailInfo = model.DetailInfo;
                    machine.DetailInfoEn = model.DetailInfoEn;

                    machine.ImageUrl = model.ImageUrl;
                    machine.AvataUrl = model.ImageUrl.Replace(@"/Images/", @"/ImageThumbs/");
                    machine.UpdateDate = DateTime.Now;

                    context.TB_MACHINERIES.Attach(machine);
                    var entry = context.Entry(machine);

                    entry.Property(x => x.Name).IsModified = true;
                    entry.Property(x => x.NameEn).IsModified = true;
                    entry.Property(x => x.Count).IsModified = true;
                    entry.Property(x => x.Year).IsModified = true;
                    entry.Property(x => x.MadeIn).IsModified = true;
                    entry.Property(x => x.MadeInEn).IsModified = true;
                    entry.Property(x => x.Power).IsModified = true;
                    entry.Property(x => x.PowerEn).IsModified = true;
                    entry.Property(x => x.DetailInfo).IsModified = true;
                    entry.Property(x => x.DetailInfoEn).IsModified = true;
                    entry.Property(x => x.ImageUrl).IsModified = true;
                    entry.Property(x => x.AvataUrl).IsModified = true;
                    entry.Property(x => x.UpdateDate).IsModified = true;

                    result = context.SaveChanges();
                }
                else
                {
                    TempData["MESSAGE"] = "Dự án không tồn tại hoặc đã bị xóa";
                    return View("EditMachineView", model);
                }
            }

            if (result == 0)
            {
                TempData["MESSAGE"] = "Hệ thống gặp vấn đề, xin thử lại sau";
                return View("EditMachineView", model);
            }

            TempData["MESSAGE"] = "Cập nhật thành công";

            return RedirectToAction("Machinery");
        }

        public ActionResult DeleteMachine(int id)
        {
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                var machine = context.TB_MACHINERIES.FirstOrDefault(x => x.Id == id);
                if (machine != null)
                {
                    context.TB_MACHINERIES.Remove(machine);
                }

                context.SaveChanges();
            }

            TempData["MESSAGE"] = "Xóa thành công";

            return RedirectToAction("Machinery");
        }

        public ActionResult Login()
        {
            LoginModel model = new LoginModel();
            model.UserName = "admin";
            model.Password = "Hello12#";
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!VerifyAccount(model.UserName))
            {
                ModelState.AddModelError(string.Empty, "Tài khoản không tồn tại");
                return View(model);
            }

            if (!VerifyPassword(model.UserName, model.Password))
            {
                ModelState.AddModelError(string.Empty, "Tài khoản hoặc mật khẩu không đúng");
                return View(model);
            }

            return RedirectToAction("Solution");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return View("Login");
        }

        private bool VerifyAccount(string userName)
        {
            bool isValid = false;
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                isValid = context.TB_USER.Any(x => x.UserName.Equals(userName.ToLower()));
            }

            return isValid;
        }

        private bool VerifyPassword(string userName, string password)
        {
            TB_USER user = new TB_USER();
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                user = context.TB_USER.FirstOrDefault(x => x.UserName.Equals(userName.ToLower()));
            }

            if (user == null) return false;
            password = BCrypt.Net.BCrypt.HashPassword(password, user.PasswordSalt);

            if (!password.Equals(user.Password)) return false;
            Session["USER_NAME"] = user.UserName;
            Session["USER_ID"] = user.UserId;
            return true;
        }

        private IEnumerable<SelectListItem> GetArticalTypeDropDown()
        {
            ICollection<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem
            {
                Text = "Dự Án",
                Value = "1"
            });

            list.Add(new SelectListItem
            {
                Text = "Tin Tức",
                Value = "2"
            });

            list.Add(new SelectListItem
            {
                Text = "Đoàn Thể",
                Value = "3"
            });

            return list;
        }

        private IEnumerable<SelectListItem> GetSolutionTypeDropDown()
        {
            ICollection<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem
            {
                Text = "Thi công cơ giới",
                Value = "1"
            });

            list.Add(new SelectListItem
            {
                Text = "Thi công xây lắp",
                Value = "2"
            });

            list.Add(new SelectListItem
            {
                Text = "Vật liệu xây dựng",
                Value = "3"
            });

            list.Add(new SelectListItem
            {
                Text = "Cửa nhôm kính",
                Value = "4"
            });

            return list;
        }
    }
}