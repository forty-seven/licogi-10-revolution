﻿using LICOGI_10_REVOLUTION.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Controllers
{
    public class FileController : Controller
    {
        //
        // GET: /File/

        public ActionResult Manager()
        {
            ICollection<TB_FILE> listFile = new List<TB_FILE>();
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                listFile = context.TB_FILE.OrderByDescending(x => x.UploadDate).ToList();
            }
            return View(listFile);
        }

        public ActionResult Gallery()
        {
            ICollection<TB_FILE> top10Files = new List<TB_FILE>();
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                top10Files = context.TB_FILE.OrderByDescending(x => x.UploadDate).Take(10).ToList();
            }
            return View(top10Files);
        }

        [HttpPost]
        public ActionResult Upload(IEnumerable<HttpPostedFileBase> listFile, string type)
        {
            int result = 0;
            string imagePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Uploads/Images/");
            string imageThumbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Uploads/ImageThumbs/");
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Uploads/Files/");

            List<TB_FILE> listToDB = new List<TB_FILE>();

            foreach (var item in listFile)
            {
                TB_FILE file = new TB_FILE();
                if (item.ContentType.Contains("image"))
                {
                    var image = Image.FromStream(item.InputStream);
                    var thumbFile = image.GetThumbnailImage(200, 200, () => false, IntPtr.Zero);

                    // Save File
                    image.Save(imagePath + item.FileName);
                    thumbFile.Save(imageThumbPath + item.FileName);

                    file.Url = @"Uploads/Images/" + item.FileName;
                    file.ThumbUrl = @"Uploads/ImageThumbs/" + item.FileName;
                }
                else
                {
                    // Save File
                    item.SaveAs(filePath + item.FileName);

                    file.Url = @"Uploads/Files/" + item.FileName;
                }
                file.Name = item.FileName;
                file.FileType = item.ContentType;
                file.UploadDate = DateTime.Now;
                listToDB.Add(file);
            }

            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                foreach (var item in listToDB)
                {
                    if (!context.TB_FILE.Any(x => x.Name.Equals(item.Name) && x.Url.Equals(item.Url)))
                    {
                        context.TB_FILE.Add(item);
                    }
                }
                result = context.SaveChanges();
            }

            if (result != 0)
            {
                string viewName = type.Equals("1") ? "_ListFileManagerPartial" : "_ListFilePartial";
                return PartialView(viewName, listToDB);
            }

            return Json(Constant.REQUEST_FAIL, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Search(string input, string type)
        {
            ICollection<TB_FILE> listResult = new List<TB_FILE>();
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                if (!string.IsNullOrEmpty(input))
                {
                    listResult = context.TB_FILE.Where(x => x.Name.Contains(input)).OrderByDescending(x => x.UploadDate).ToList();
                }
                else
                {
                    listResult = context.TB_FILE.OrderByDescending(x => x.UploadDate).Take(10).ToList();
                }
            }

            string viewName = type.Equals("1") ? "_ListFileManagerPartial" : "_ListFilePartial";
            return PartialView(viewName, listResult);
        }

        public ActionResult Delete(int id)
        {
            using (LOGICO_10_REVOLUTIONEntities context = new LOGICO_10_REVOLUTIONEntities())
            {
                if (!context.TB_FILE.Any(x => x.Id == id))
                {
                    Response.Write(Constant.REQUEST_FAIL);
                    return null;
                }

                var deleteItem = context.TB_FILE.Where(x => x.Id == id).FirstOrDefault();

                if (deleteItem != null)
                {
                    // Delete file on hard disk
                    string fileUri = AppDomain.CurrentDomain.BaseDirectory + "/" + deleteItem.Url;
                    if (System.IO.File.Exists(fileUri))
                    {
                        System.IO.File.Delete(fileUri);
                    }

                    if (deleteItem.FileType.Contains("image"))
                    {
                        string fileUriThumb = AppDomain.CurrentDomain.BaseDirectory + "/" + deleteItem.ThumbUrl;

                        if (System.IO.File.Exists(fileUriThumb))
                        {
                            System.IO.File.Delete(fileUriThumb);
                        }
                    }

                    // Delete info in database
                    context.TB_FILE.Remove(deleteItem);
                }

                context.SaveChanges();
            }

            Response.Write(Constant.REQUEST_SUCCESS);
            return null;
        }
    }
}