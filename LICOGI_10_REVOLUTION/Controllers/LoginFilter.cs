﻿using System.Web.Mvc;
using System.Web.Routing;

namespace LICOGI_10_REVOLUTION.Controllers
{
    public class LoginFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = filterContext.HttpContext.Session;

            if (session["USER_ID"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Admin", action = "Login" }));
            }

            base.OnActionExecuting(filterContext);
        }
    }
}