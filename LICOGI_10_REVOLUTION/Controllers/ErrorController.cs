﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult ErrorInfo(string message)
        {
            TempData["MESSAGE"] = message;
            return View();
        }

    }
}
