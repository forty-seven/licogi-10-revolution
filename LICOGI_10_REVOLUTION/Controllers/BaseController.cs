﻿using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Controllers
{
    public class BaseController : Controller
    {
        protected override void ExecuteCore()
        {
            int culture;
            if (Session?["CurrentCulture"] == null)
            {
                int.TryParse(System.Configuration.ConfigurationManager.AppSettings["Culture"], out culture);
                var httpSessionStateBase = Session;
                if (httpSessionStateBase != null) httpSessionStateBase["CurrentCulture"] = culture;
            }
            else
            {
                culture = (int)Session["CurrentCulture"];
            }
            // calling CultureHelper class properties for setting
            CultureHelper.CurrentCulture = culture;

            base.ExecuteCore();
        }

        protected override bool DisableAsyncSupport => true;
    }
}