﻿$(function () {
    $('#uploadButton').click(function () {
        //// Get form data
        var formData = new FormData($('#uploadFile')[0]);  // use "[0]" <- important
        $('body').addClass('disabledLoad');
        $.ajax({
            type: 'POST',
            url: upLoadUrl,
            //dataType: "text json",
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                $('.search-result').html('');
                $('.search-result').html(result);
                $('#uploadFile input[type="file"]').val('');
                $('body').removeClass('disabledLoad');
                console.log(result);
            },
            error: function (result) {
                console.log(result)
            }
        });
    });


    $('#searchFile').click(function () {
        $('#gallery').addClass('disabledLoad');
        var input = $('#searchInput').val();
        $.ajax({
            type: 'POST',
            url: searchUrl,
            data: { 'input': input, 'type':"2" },
            success: function (result) {
                $('.search-result').html('');
                $('.search-result').html(result);
                $('#gallery').removeClass('disabledLoad');
            },
            error: function (result) {
            }
        });
    });
});