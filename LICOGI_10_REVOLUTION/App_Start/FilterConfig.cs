﻿using System.Web;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}