﻿using System.Web.Mvc;
using System.Web.Routing;

namespace LICOGI_10_REVOLUTION
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                 name: "Login",
                 url: "login",
                 defaults: new
                 {
                     controller = "Admin",
                     action = "Login"
                 }
            );

            // Admin controller
            routes.MapRoute(
               name: "Admin",
               url: "admin/{action}/{id}",
               defaults: new
               {
                   controller = "Admin",
                   action = "Solution",
                   id = UrlParameter.Optional
               }
            );

            // File controller
            routes.MapRoute(
               name: "File",
               url: "file/{action}/{id}",
               defaults: new
               {
                   controller = "File",
                   action = "Manager",
                   id = UrlParameter.Optional
               }
            );

            routes.MapRoute(
               name: "Artical",
               url: "{articalType}/{id}/{url}",
               defaults: new
               {
                   controller = "Home",
                   action = "ChiTietBaiViet",
               }
           );
            // remove controller in url
            routes.MapRoute(
                 name: "RemoveURL",
                 url: "{action}/{id}",
                 defaults: new
                 {
                     controller = "Home",
                     action = "Index",
                     id = UrlParameter.Optional
                 }
            );

            // Default of mvc
            //routes.MapRoute(
            //    name: "Home",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new
            //    {
            //        controller = "Home",
            //        action = "Index",
            //        id = UrlParameter.Optional
            //    }
            //);
        }
    }
}