//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LICOGI_10_REVOLUTION
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_ARTICAL
    {
        public int Id { get; set; }
        public byte ArticalType { get; set; }
        public Nullable<byte> SolutionType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ContentText { get; set; }
        public string FriendlyUrl { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<short> DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        public string AvataUrl { get; set; }
        public string TitleEn { get; set; }
        public string DescriptionEn { get; set; }
        public string ContentTextEn { get; set; }
        public string FriendlyUrlEn { get; set; }
        public Nullable<bool> IsProjectDone { get; set; }
    }
}
