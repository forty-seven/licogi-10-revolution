//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LICOGI_10_REVOLUTION
{
    using System;
    using System.Collections.Generic;
    
    public partial class TB_MACHINERIES
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<short> Count { get; set; }
        public Nullable<short> Year { get; set; }
        public string MadeIn { get; set; }
        public string Power { get; set; }
        public string DetailInfo { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string NameEn { get; set; }
        public string MadeInEn { get; set; }
        public string PowerEn { get; set; }
        public string DetailInfoEn { get; set; }
        public string AvataUrl { get; set; }
        public string ImageUrl { get; set; }
    }
}
