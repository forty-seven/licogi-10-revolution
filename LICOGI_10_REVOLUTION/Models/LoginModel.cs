﻿using System.ComponentModel.DataAnnotations;

namespace LICOGI_10_REVOLUTION.Models
{
    public class LoginModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}