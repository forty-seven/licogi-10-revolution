﻿using System.ComponentModel.DataAnnotations;

namespace LICOGI_10_REVOLUTION.Models
{
    public class HighLightModel
    {
        public int Id { get; set; }

        [Display(Name = "Tên")]
        public string Name { get; set; }

        [Display(Name = "Đường Dẫn Hình Ảnh")]
        public string ImageUrl { get; set; }

        [Display(Name = "Đường Dẫn Bài Viết")]
        public string ArticalUrl { get; set; }

        [Display(Name = "Lời Giới thiệu")]
        public string Message { get; set; }

        [Display(Name = "Vị Trí Giới Thiệu (Nhập 1 = bên trái, 2 = bên phải)")]
        public byte MessagePosition { get; set; }

        [Display(Name = "Lên Trang Nhất")]
        public bool ToTheTop { get; set; }
    }
}