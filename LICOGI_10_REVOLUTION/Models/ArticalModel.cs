﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Models
{
    public class ArticalModel
    {
        public int Id { get; set; }

        // 1 = project, 2 = new, 3 = youth
        [Required(ErrorMessage = "Cái này phải nhập")]
        public byte ArticalType { get; set; }

        public IEnumerable<SelectListItem> ListArticalType { get; set; }

        // When ArticalType = 1, this will in use
        [Required(ErrorMessage = "Cái này phải nhập")]
        public byte? SolutionType { get; set; }

        public IEnumerable<SelectListItem> ListSolutionType { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        public string Title { get; set; }

        public string TitleEn { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        public string AvataUrl { get; set; }

        public string Description { get; set; }

        public string DescriptionEn { get; set; }

        [AllowHtml]
        public string ContentText { get; set; }

        [AllowHtml]
        public string ContentTextEn { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        [RegularExpression(@"([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$", ErrorMessage = "Cái này phải có định dạng địa chỉ web, ví dụ : du-an-song-tranh")]
        public string FriendlyUrl { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        [RegularExpression(@"([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$", ErrorMessage = "Cái này phải có định dạng địa chỉ web, ví dụ : du-an-song-tranh")]
        public string FriendlyUrlEn { get; set; }

        [DataType(DataType.Currency, ErrorMessage = "Cái này nhập số thôi")]
        public short? DisplayOrder { get; set; }

        public bool IsProjectDone { get; set; }

        public bool IsActive { get; set; }
    }
}