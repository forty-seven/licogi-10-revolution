﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Models
{
    public class SolutionModel
    {
        public List<PageModel> ListPageModel { get; set; }
        public List<PageModel> listPage { get; set; }
    }

    public class PageModel
    {
        public int Id { get; set; }

        [Display(Name = "Tên Trang")]
        public string Name { get; set; }

        [AllowHtml]
        [Display(Name = "Nội Dung")]
        public string ContentText { get; set; }

        [AllowHtml]
        [Display(Name = "Nội Dung Tiếng Anh")]
        public string ContentTextEn { get; set; }
    }
}