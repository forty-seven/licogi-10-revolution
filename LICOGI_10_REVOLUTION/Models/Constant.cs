﻿namespace LICOGI_10_REVOLUTION.Models
{
    public class Constant
    {
        public const string REQUEST_SUCCESS = "SUCCESS";
        public const string REQUEST_FAIL = "FAIL";

        public static int THI_CONG_CO_GIOI = 1;
        public static int THI_CONG_XAY_LAP = 2;
        public static int VAT_LIEU_XAY_DUNG = 3;
        public static int CUA_KINH_SON_TINH_DIEN = 4;
        public static int VAN_BAN_PHAP_QUY = 5;
        public static int GIOITHIEU = 6;
        public static int THIET_BI_THI_CONG = 7;
        public static int DU_AN = 8;

        public static int ARTICAL_PROJECT = 1;
        public static int ARTICAL_NEWS = 2;
        public static int ARTICAL_YOUTH = 3;
    }
}