﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LICOGI_10_REVOLUTION.Models
{
    public class MachineModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        public string Name { get; set; }

        public string NameEn { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        public int Count { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        public int Year { get; set; }

        public string MadeIn { get; set; }
        public string MadeInEn { get; set; }

        public string Power { get; set; }
        public string PowerEn { get; set; }

        [AllowHtml]
        public string DetailInfo { get; set; }

        [AllowHtml]
        public string DetailInfoEn { get; set; }

        public string AvataUrl { get; set; }

        [Required(ErrorMessage = "Cái này phải nhập")]
        public string ImageUrl { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}